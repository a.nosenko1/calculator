import parser.CalcParser;
import parser.CommandAndItsArgs;
import services.*;

import java.util.LinkedList;

public final class Main {
    public static void main(String[] args) {
        CommandAndItsArgs commandAndItsArgs = new CommandAndItsArgs();
        commandAndItsArgs.commandName = "PUSH";
        commandAndItsArgs.args = new LinkedList<>();
        commandAndItsArgs.args.add("a");
        AnalyzerExistingCommands.analyze();
        iStorage storage = new Storage();
        storage.pushStack(1);
        storage.defineWordAsNumber("b", 5);
        try {
            PreCompValidator.isValid(commandAndItsArgs, storage);
        } catch (ValidatorException ex){
            System.out.println(ex.getMessage());
        }

    }
}