grammar CalculatorGrammar;
// parser

line                : (command WHITESPACE (argument (WHITESPACE|NEWLINE))+ )|
                      (command WHITESPACE argument WHITESPACE* NEWLINE)|
                      (command WHITESPACE* NEWLINE);

comment             : '#' WHITESPACE* (WORD|NUMBER)* NEWLINE ;
command             : PUSH | POP | PRINT | DEFINE | SQRT ;
argument            : NUMBER | WORD ;

// lexer
fragment P          : ('P'|'p') ;
fragment U          : ('U'|'u') ;
fragment S          : ('S'|'s') ;
fragment H          : ('H'|'h') ;
fragment O          : ('O'|'o') ;

fragment Q          : ('Q'|'q') ;
fragment R          : ('R'|'r') ;
fragment T          : ('T'|'t') ;

fragment I          : ('I'|'i') ;
fragment N          : ('N'|'n') ;

fragment D          : ('D'|'d') ;
fragment E          : ('E'|'e') ;
fragment F          : ('F'|'f') ;
fragment LOWERCASE  : [a-z] ;
fragment UPPERCASE  : [A-Z] ;
fragment DIGITS     : '1'..'9' '0'..'9'*;

NUMBER              : '-'? INT '.' [0-9]+ | '-'? INT;

INT                 : '0' | [1-9] [0-9]*;


PUSH                : P U S H ;
POP                 : P O P ;
DEFINE              : D E F I N E ;
SQRT                : S Q R T ;
PRINT               : P R I N T ;

WORD                : (LOWERCASE | UPPERCASE | '_')+ ;
WHITESPACE          : (' ' | '\t')+ ;
NEWLINE             : ('\r'? '\n' | '\r' )+ ;


