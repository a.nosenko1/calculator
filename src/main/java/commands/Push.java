package commands;

import parser.CommandAndItsArgs;
import services.iStorage;

public class Push implements iCommand{
    @Override
    public int getCountArgs() {
        return 1;
    }

    @Override
    public int getRequiredCountArgsOnStack() {
        return 0;
    }

    @Override
    public void execute(CommandAndItsArgs commandAndItsArgs, iStorage storage) {

    }
}
