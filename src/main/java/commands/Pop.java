package commands;

import parser.CommandAndItsArgs;
import services.iStorage;

public class Pop implements iCommand{

    @Override
    public void execute(CommandAndItsArgs commandAndItsArgs, iStorage storage) {

    }

    @Override
    public int getRequiredCountArgsOnStack() {
        return 1;
    }

    @Override
    public int getCountArgs() {
        return 0;
    }
}
