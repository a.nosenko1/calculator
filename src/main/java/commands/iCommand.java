package commands;

import parser.CommandAndItsArgs;
import services.iStorage;

public interface iCommand {
    int getCountArgs();
    int getRequiredCountArgsOnStack();

    void execute(CommandAndItsArgs commandAndItsArgs, iStorage storage);
}
