// Generated from /home/vatson/IdeaProjects/calculator/src/main/java/antlr4/CalculatorGrammar.g4 by ANTLR 4.9.1
package antlr;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class CalculatorGrammarLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.9.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, NUMBER=2, INT=3, PUSH=4, POP=5, DEFINE=6, SQRT=7, PRINT=8, WORD=9, 
		WHITESPACE=10, NEWLINE=11;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"T__0", "P", "U", "S", "H", "O", "Q", "R", "T", "I", "N", "D", "E", "F", 
			"LOWERCASE", "UPPERCASE", "DIGITS", "NUMBER", "INT", "PUSH", "POP", "DEFINE", 
			"SQRT", "PRINT", "WORD", "WHITESPACE", "NEWLINE"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'#'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, "NUMBER", "INT", "PUSH", "POP", "DEFINE", "SQRT", "PRINT", 
			"WORD", "WHITESPACE", "NEWLINE"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public CalculatorGrammarLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "CalculatorGrammar.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\r\u00aa\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\3\2\3\2\3\3\3\3\3\4\3\4\3\5\3\5\3\6\3\6"+
		"\3\7\3\7\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3\13\3\f\3\f\3\r\3\r\3\16\3\16\3"+
		"\17\3\17\3\20\3\20\3\21\3\21\3\22\3\22\7\22\\\n\22\f\22\16\22_\13\22\3"+
		"\23\5\23b\n\23\3\23\3\23\3\23\6\23g\n\23\r\23\16\23h\3\23\5\23l\n\23\3"+
		"\23\5\23o\n\23\3\24\3\24\3\24\7\24t\n\24\f\24\16\24w\13\24\5\24y\n\24"+
		"\3\25\3\25\3\25\3\25\3\25\3\26\3\26\3\26\3\26\3\27\3\27\3\27\3\27\3\27"+
		"\3\27\3\27\3\30\3\30\3\30\3\30\3\30\3\31\3\31\3\31\3\31\3\31\3\31\3\32"+
		"\3\32\3\32\6\32\u0099\n\32\r\32\16\32\u009a\3\33\6\33\u009e\n\33\r\33"+
		"\16\33\u009f\3\34\5\34\u00a3\n\34\3\34\3\34\6\34\u00a7\n\34\r\34\16\34"+
		"\u00a8\2\2\35\3\3\5\2\7\2\t\2\13\2\r\2\17\2\21\2\23\2\25\2\27\2\31\2\33"+
		"\2\35\2\37\2!\2#\2%\4\'\5)\6+\7-\b/\t\61\n\63\13\65\f\67\r\3\2\24\4\2"+
		"RRrr\4\2WWww\4\2UUuu\4\2JJjj\4\2QQqq\4\2SSss\4\2TTtt\4\2VVvv\4\2KKkk\4"+
		"\2PPpp\4\2FFff\4\2GGgg\4\2HHhh\3\2c|\3\2C\\\3\2\62;\3\2\63;\4\2\13\13"+
		"\"\"\2\u00a7\2\3\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2"+
		"\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2"+
		"\2\2\39\3\2\2\2\5;\3\2\2\2\7=\3\2\2\2\t?\3\2\2\2\13A\3\2\2\2\rC\3\2\2"+
		"\2\17E\3\2\2\2\21G\3\2\2\2\23I\3\2\2\2\25K\3\2\2\2\27M\3\2\2\2\31O\3\2"+
		"\2\2\33Q\3\2\2\2\35S\3\2\2\2\37U\3\2\2\2!W\3\2\2\2#Y\3\2\2\2%n\3\2\2\2"+
		"\'x\3\2\2\2)z\3\2\2\2+\177\3\2\2\2-\u0083\3\2\2\2/\u008a\3\2\2\2\61\u008f"+
		"\3\2\2\2\63\u0098\3\2\2\2\65\u009d\3\2\2\2\67\u00a6\3\2\2\29:\7%\2\2:"+
		"\4\3\2\2\2;<\t\2\2\2<\6\3\2\2\2=>\t\3\2\2>\b\3\2\2\2?@\t\4\2\2@\n\3\2"+
		"\2\2AB\t\5\2\2B\f\3\2\2\2CD\t\6\2\2D\16\3\2\2\2EF\t\7\2\2F\20\3\2\2\2"+
		"GH\t\b\2\2H\22\3\2\2\2IJ\t\t\2\2J\24\3\2\2\2KL\t\n\2\2L\26\3\2\2\2MN\t"+
		"\13\2\2N\30\3\2\2\2OP\t\f\2\2P\32\3\2\2\2QR\t\r\2\2R\34\3\2\2\2ST\t\16"+
		"\2\2T\36\3\2\2\2UV\t\17\2\2V \3\2\2\2WX\t\20\2\2X\"\3\2\2\2Y]\4\63;\2"+
		"Z\\\4\62;\2[Z\3\2\2\2\\_\3\2\2\2][\3\2\2\2]^\3\2\2\2^$\3\2\2\2_]\3\2\2"+
		"\2`b\7/\2\2a`\3\2\2\2ab\3\2\2\2bc\3\2\2\2cd\5\'\24\2df\7\60\2\2eg\t\21"+
		"\2\2fe\3\2\2\2gh\3\2\2\2hf\3\2\2\2hi\3\2\2\2io\3\2\2\2jl\7/\2\2kj\3\2"+
		"\2\2kl\3\2\2\2lm\3\2\2\2mo\5\'\24\2na\3\2\2\2nk\3\2\2\2o&\3\2\2\2py\7"+
		"\62\2\2qu\t\22\2\2rt\t\21\2\2sr\3\2\2\2tw\3\2\2\2us\3\2\2\2uv\3\2\2\2"+
		"vy\3\2\2\2wu\3\2\2\2xp\3\2\2\2xq\3\2\2\2y(\3\2\2\2z{\5\5\3\2{|\5\7\4\2"+
		"|}\5\t\5\2}~\5\13\6\2~*\3\2\2\2\177\u0080\5\5\3\2\u0080\u0081\5\r\7\2"+
		"\u0081\u0082\5\5\3\2\u0082,\3\2\2\2\u0083\u0084\5\31\r\2\u0084\u0085\5"+
		"\33\16\2\u0085\u0086\5\35\17\2\u0086\u0087\5\25\13\2\u0087\u0088\5\27"+
		"\f\2\u0088\u0089\5\33\16\2\u0089.\3\2\2\2\u008a\u008b\5\t\5\2\u008b\u008c"+
		"\5\17\b\2\u008c\u008d\5\21\t\2\u008d\u008e\5\23\n\2\u008e\60\3\2\2\2\u008f"+
		"\u0090\5\5\3\2\u0090\u0091\5\21\t\2\u0091\u0092\5\25\13\2\u0092\u0093"+
		"\5\27\f\2\u0093\u0094\5\23\n\2\u0094\62\3\2\2\2\u0095\u0099\5\37\20\2"+
		"\u0096\u0099\5!\21\2\u0097\u0099\7a\2\2\u0098\u0095\3\2\2\2\u0098\u0096"+
		"\3\2\2\2\u0098\u0097\3\2\2\2\u0099\u009a\3\2\2\2\u009a\u0098\3\2\2\2\u009a"+
		"\u009b\3\2\2\2\u009b\64\3\2\2\2\u009c\u009e\t\23\2\2\u009d\u009c\3\2\2"+
		"\2\u009e\u009f\3\2\2\2\u009f\u009d\3\2\2\2\u009f\u00a0\3\2\2\2\u00a0\66"+
		"\3\2\2\2\u00a1\u00a3\7\17\2\2\u00a2\u00a1\3\2\2\2\u00a2\u00a3\3\2\2\2"+
		"\u00a3\u00a4\3\2\2\2\u00a4\u00a7\7\f\2\2\u00a5\u00a7\7\17\2\2\u00a6\u00a2"+
		"\3\2\2\2\u00a6\u00a5\3\2\2\2\u00a7\u00a8\3\2\2\2\u00a8\u00a6\3\2\2\2\u00a8"+
		"\u00a9\3\2\2\2\u00a98\3\2\2\2\20\2]ahknux\u0098\u009a\u009f\u00a2\u00a6"+
		"\u00a8\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}