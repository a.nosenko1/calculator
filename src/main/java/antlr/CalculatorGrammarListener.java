// Generated from /home/vatson/IdeaProjects/calculator/src/main/java/antlr4/CalculatorGrammar.g4 by ANTLR 4.9.1
package antlr;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link CalculatorGrammarParser}.
 */
public interface CalculatorGrammarListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link CalculatorGrammarParser#line}.
	 * @param ctx the parse tree
	 */
	void enterLine(CalculatorGrammarParser.LineContext ctx);
	/**
	 * Exit a parse tree produced by {@link CalculatorGrammarParser#line}.
	 * @param ctx the parse tree
	 */
	void exitLine(CalculatorGrammarParser.LineContext ctx);
	/**
	 * Enter a parse tree produced by {@link CalculatorGrammarParser#comment}.
	 * @param ctx the parse tree
	 */
	void enterComment(CalculatorGrammarParser.CommentContext ctx);
	/**
	 * Exit a parse tree produced by {@link CalculatorGrammarParser#comment}.
	 * @param ctx the parse tree
	 */
	void exitComment(CalculatorGrammarParser.CommentContext ctx);
	/**
	 * Enter a parse tree produced by {@link CalculatorGrammarParser#command}.
	 * @param ctx the parse tree
	 */
	void enterCommand(CalculatorGrammarParser.CommandContext ctx);
	/**
	 * Exit a parse tree produced by {@link CalculatorGrammarParser#command}.
	 * @param ctx the parse tree
	 */
	void exitCommand(CalculatorGrammarParser.CommandContext ctx);
	/**
	 * Enter a parse tree produced by {@link CalculatorGrammarParser#argument}.
	 * @param ctx the parse tree
	 */
	void enterArgument(CalculatorGrammarParser.ArgumentContext ctx);
	/**
	 * Exit a parse tree produced by {@link CalculatorGrammarParser#argument}.
	 * @param ctx the parse tree
	 */
	void exitArgument(CalculatorGrammarParser.ArgumentContext ctx);
}