// Generated from /home/vatson/IdeaProjects/calculator/src/main/java/antlr4/CalculatorGrammar.g4 by ANTLR 4.9.1
package antlr;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link CalculatorGrammarParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface CalculatorGrammarVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link CalculatorGrammarParser#line}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLine(CalculatorGrammarParser.LineContext ctx);
	/**
	 * Visit a parse tree produced by {@link CalculatorGrammarParser#comment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComment(CalculatorGrammarParser.CommentContext ctx);
	/**
	 * Visit a parse tree produced by {@link CalculatorGrammarParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCommand(CalculatorGrammarParser.CommandContext ctx);
	/**
	 * Visit a parse tree produced by {@link CalculatorGrammarParser#argument}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgument(CalculatorGrammarParser.ArgumentContext ctx);
}