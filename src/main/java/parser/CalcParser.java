package parser;

import antlr.CalculatorGrammarLexer;
import antlr.CalculatorGrammarParser;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.misc.ParseCancellationException;

public class CalcParser {

    public static CommandAndItsArgs parse(String string) throws ParseCancellationException {
        /* Создает структуру CommandAndItsArgs
           Верент null и исключение, если ошибка парсинга */

        if (!string.isEmpty() && "#".equals(Character.toString(string.charAt(0)))) return null; // костыль комментария

        CharStream in = CharStreams.fromString(string);
        CalculatorGrammarLexer lexer = new CalculatorGrammarLexer(in);
        lexer.removeErrorListeners();
        lexer.addErrorListener(ThrowingErrorListener.INSTANCE);

        CommandAndItsArgs commandAndItsArgs = new CommandAndItsArgs();

        CalculatorGrammarParser parser = new CalculatorGrammarParser(new CommonTokenStream(lexer));
        parser.removeErrorListeners();
        parser.addErrorListener(ThrowingErrorListener.INSTANCE);
        MyListener listener = new MyListener(commandAndItsArgs);
        parser.addParseListener(listener);

        parser.line();

        if (listener.listenerState == MyListener.ListenerState.FAIL) // невалидная строка
            return null;

        return commandAndItsArgs;
    }

}
