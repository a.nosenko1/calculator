package parser;

import java.util.LinkedList;

public class CommandAndItsArgs{
    public CommandAndItsArgs(){
        args = new LinkedList<>();
        commandName = null;
    }
    public String commandName;
    public LinkedList<String> args;
}
