package parser;

import antlr.CalculatorGrammarBaseListener;
import antlr.CalculatorGrammarParser;
import org.antlr.v4.runtime.tree.ErrorNode;

public class MyListener extends CalculatorGrammarBaseListener {
    public enum ListenerState {FAIL, SUCCESSFUL}
    public ListenerState listenerState;
    public MyListener(CommandAndItsArgs commandAndItsArgs){
        this.commandAndItsArgs = commandAndItsArgs;
    }
    private final CommandAndItsArgs commandAndItsArgs;

    @Override
    public final void visitErrorNode(ErrorNode node) {
        super.visitErrorNode(node);
    }

    @Override public final void exitLine(CalculatorGrammarParser.LineContext ctx) {
        /* если ctx.command() == null, то парсер выкинет исключение,
           поэтому обрабатываю только хороший случай */
        listenerState = ListenerState.FAIL;
        if (ctx.command() != null) {
            listenerState = ListenerState.SUCCESSFUL;
            this.commandAndItsArgs.commandName = ctx.command().getText().toUpperCase();
            for (int i = 0; i < ctx.argument().size(); i++)
                this.commandAndItsArgs.args.add(ctx.argument(i).getText());
        }

    }

}
