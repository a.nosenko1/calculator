package services;

import commands.iCommand;
import org.reflections.Reflections;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Set;

public class AnalyzerExistingCommands {
    public final static String path = "src/main/java/services/calc.properties";

    public static void analyze(){
        Reflections reflections = new Reflections("commands");
        Set<Class<? extends iCommand>> set = reflections.getSubTypesOf(iCommand.class);

        try (BufferedWriter writter = new BufferedWriter(new FileWriter(path))) {
            write(writter, "Command", "full class name",
                    "countArgs", "requiredCountArgsOnStack");
            for (Class<? extends iCommand> command : set){
                //TODO: сделать это без newInstance
                int countArgs = command.getDeclaredConstructor().newInstance().getCountArgs();
                int requiredCountArgsOnStack = command.getDeclaredConstructor().newInstance().getRequiredCountArgsOnStack();

                // а как то так
                //Method method = command.getDeclaredMethod("getRequiredCountArgsOnStack");
                //method.setAccessible(true);
                //Object countAgs = method.invoke(null); -- ??

                String[] commandSplit = command.toString().split("\\.");
                String shortName = commandSplit[commandSplit.length-1];
                String fullName = command.toString();

                write(writter, shortName, fullName, Integer.toString(countArgs), Integer.toString(requiredCountArgsOnStack));
            }
        }
        catch (IOException | NoSuchMethodException | IllegalAccessException |
                InvocationTargetException | InstantiationException e) {
            e.printStackTrace();
        }
    }

    private static void write(BufferedWriter writter, String shortName,
                       String fullName, String countArgs, String requiredCountArgsOnStack) throws IOException {
        writter.write(shortName.toUpperCase() + " =");
        for (int i = 1; i < COUNT_PROPERTIES_PARAMETERS; i++) {
            if (FULL_NAME_POSITION == i){
                writter.write(fullName + "|");
            }
            if (COUNT_ARGS_POSITION == i){
                writter.write(countArgs + "|");
            }
            if (COUNT_ARGS_ON_STACK_POSITION == i){
                writter.write(requiredCountArgsOnStack + "|");
            }
        }
        writter.newLine();
    }

    public final static int COUNT_PROPERTIES_PARAMETERS = 4;
    public final static int SHORT_NAME_POSITION = 0; // always
    public final static int FULL_NAME_POSITION = 3;
    public final static int COUNT_ARGS_POSITION = 2;
    public final static int COUNT_ARGS_ON_STACK_POSITION = 1;
}
