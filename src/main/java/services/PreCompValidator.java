package services;

import parser.CommandAndItsArgs;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public final class PreCompValidator {
    /* Исследует CommandAndItsArgs, проверяет:
    1) Команда существует (доп проверка после парсера)
    2) Верное число аргументов
    3) На стеке достаточно аргументов
    4) Параметры есть в дефайн листе (для всех кроме DEFINE)
    Если что-то не так - исключение и false
    * */
    public static boolean isValid(CommandAndItsArgs commandAndItsArgs, iStorage storage) throws ValidatorException {
        String configPath = AnalyzerExistingCommands.path;
        Properties properties = new Properties();
        String[] parameters;
        try {
            properties.load(new FileInputStream(configPath));
        }catch (IOException ex){
            ex.printStackTrace();
        }
        // 1) Команда существует
        if (!properties.containsKey(commandAndItsArgs.commandName)){
            throw new ValidatorException("Command \"" + commandAndItsArgs.commandName + "\" doesn't exist");
        }
        parameters = properties.getProperty(commandAndItsArgs.commandName).split("\\|");
        // 2) Верное число аргументов
        int expectedCountOfArgs = Integer.parseInt(parameters[AnalyzerExistingCommands.COUNT_ARGS_POSITION-1]); // ибо тут парсится только часть со значением, ключ не считается
        if (expectedCountOfArgs != commandAndItsArgs.args.size()){
            throw new ValidatorException("For command \"" + commandAndItsArgs.commandName +
                    "\" expected " + expectedCountOfArgs + " arguments, detected " + commandAndItsArgs.args.size());
        }
        // 3) На стеке достаточно аргументов
        int expectedCountOfStackElements = Integer.parseInt(
                parameters[AnalyzerExistingCommands.COUNT_ARGS_ON_STACK_POSITION-1]);
        if (storage.getStackSize() < expectedCountOfStackElements){
            throw new ValidatorException("For command \"" + commandAndItsArgs.commandName +
                    "\" expected " + expectedCountOfStackElements +
                    " elements on stack, but stack contains " + storage.getStackSize());
        }
        // 4) Параметры есть в дефайн листе
        if (!"DEFINE".equals(commandAndItsArgs.commandName)){ // <----- вооот это мне не нравится
            for (int i = 0; i < commandAndItsArgs.args.size(); i++) {
                String arg = commandAndItsArgs.args.get(i);
                try {
                    Integer.parseInt(arg);          // проверка на int
                } catch (NumberFormatException e) {
                    try {
                        Double.parseDouble(arg);    // проверка на double
                    } catch (NumberFormatException e1) {
                        // если мы тут, значит дефайн должен быть в листе
                        if (!storage.isDefined(arg)){
                            throw new ValidatorException("For argument \"" + arg +
                                    "\" define doesn't exist");
                        }
                    }
                }
            }
        }

        return true;
    }

}
