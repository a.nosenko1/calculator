package services;

public interface iStorage {
    void    pushStack(double number);
    double  popStack() throws Exception;
    double  peekStack() throws Exception;
    int     getStackSize();
    void    defineWordAsNumber(String word, double number);
    double  getDefinedValue(String word) throws Exception;
    boolean isDefined(String word);
}
