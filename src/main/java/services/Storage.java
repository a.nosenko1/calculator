package services;

import java.util.*;

public final class Storage implements iStorage {
    @Override
    public final void pushStack(double number){
        stack.push(number);
    }
    @Override
    public final double popStack() throws Exception {
        if (stack.size() < 1)
            throw new Exception("Stack is empty");
        return stack.pop();
    }
    @Override
    public final double peekStack() throws Exception {
        if (stack.size() < 1)
            throw new Exception("Stack is empty");
        return stack.peek();
    }
    @Override
    public int getStackSize() {
        return stack.size();
    }
    @Override
    public final void defineWordAsNumber(String word, double number) {
        definesList.put(word, number);
    }
    @Override
    public final double getDefinedValue(String word) throws Exception {
        if (isDefined(word))
            return definesList.get(word);
        else {
            throw new Exception("NO DEFINE VALUE FOR THIS KEY");
        }
    }
    @Override
    public boolean isDefined(String word) {
        return definesList.containsKey(word);
    }

    public Storage(){
        this.stack = new Stack<>();
        this.definesList = new HashMap<String, Double>();
    }
    private final Stack<Double> stack;
    private final HashMap<String, Double> definesList;
}
