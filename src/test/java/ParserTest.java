import parser.CalcParser;
import parser.CommandAndItsArgs;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.junit.Test;

import java.util.Locale;

public class ParserTest {
    @Test
    public void parse(){
        // correct strings

        CommandAndItsArgs commandAndItsArgs = CalcParser.parse("PUSH 5\n");
        assert ("PUSH".equals(commandAndItsArgs.commandName));
        assert (commandAndItsArgs.args.size() == 1);
        assert ("5".equals(commandAndItsArgs.args.get(0)));

        commandAndItsArgs = CalcParser.parse("POP\n");
        assert ("POP".equals(commandAndItsArgs.commandName));
        assert (commandAndItsArgs.args.size() == 0);

        commandAndItsArgs = CalcParser.parse("PUSH a\n");
        assert ("PUSH".equals(commandAndItsArgs.commandName));
        assert (commandAndItsArgs.args.size() == 1);
        assert ("a".equals(commandAndItsArgs.args.get(0)));

        commandAndItsArgs = CalcParser.parse("SQRT 25\n");
        assert ("SQRT".equals(commandAndItsArgs.commandName));
        assert (commandAndItsArgs.args.size() == 1);
        assert ("25".equals(commandAndItsArgs.args.get(0)));

        commandAndItsArgs = CalcParser.parse("PRINT a\n");
        assert ("PRINT".equals(commandAndItsArgs.commandName));
        assert (commandAndItsArgs.args.size() == 1);
        assert ("a".equals(commandAndItsArgs.args.get(0)));

        commandAndItsArgs = CalcParser.parse("DEFINE a 5\n");
        assert ("DEFINE".equals(commandAndItsArgs.commandName));
        assert (commandAndItsArgs.args.size() == 2);
        assert ("a".equals(commandAndItsArgs.args.get(0)));
        assert ("5".equals(commandAndItsArgs.args.get(1)));

        commandAndItsArgs = CalcParser.parse("PUSH Taschenrechner\n");
        assert ("PUSH".equals(commandAndItsArgs.commandName));
        assert (commandAndItsArgs.args.size() == 1);
        assert ("Taschenrechner".equals(commandAndItsArgs.args.get(0)));

        commandAndItsArgs = CalcParser.parse("PUSH a 5 f\n"); // сколько угодно аргументов
        assert ("PUSH".equals(commandAndItsArgs.commandName));
        assert (commandAndItsArgs.args.size() == 3);
        assert ("a".equals(commandAndItsArgs.args.get(0)));
        assert ("5".equals(commandAndItsArgs.args.get(1)));
        assert ("f".equals(commandAndItsArgs.args.get(2)));

        // comment
        commandAndItsArgs = CalcParser.parse("#\n");
        assert (commandAndItsArgs == null);
        commandAndItsArgs = CalcParser.parse("# dfsefsef\n");
        assert (commandAndItsArgs == null);
        commandAndItsArgs = CalcParser.parse("#sdfesf\n");
        assert (commandAndItsArgs == null);

        // incorrect strings
        try {
            commandAndItsArgs = CalcParser.parse("PUSHa a 5\n");
            assert (commandAndItsArgs == null);
        }
        catch (ParseCancellationException ex){
            System.out.println(ex.getMessage());
        }
        try {
            commandAndItsArgs = CalcParser.parse("PU SH a\n");
        }
        catch (ParseCancellationException ex){
            System.out.println(ex.getMessage());
        }
        try {
            commandAndItsArgs = CalcParser.parse("\n");
        }
        catch (ParseCancellationException ex){
            System.out.println(ex.getMessage());
        }
        try {
            commandAndItsArgs = CalcParser.parse("");
        }
        catch (ParseCancellationException ex){
            System.out.println(ex.getMessage());
        }
        try {
            commandAndItsArgs = CalcParser.parse("Hello world\n");
        }
        catch (ParseCancellationException ex){
            System.out.println(ex.getMessage());
        }
        try {
            commandAndItsArgs = CalcParser.parse("Taschenrechner\n");
        }
        catch (ParseCancellationException ex){
            System.out.println(ex.getMessage());
        }
        try {
            commandAndItsArgs = CalcParser.parse("PUSH asds!ad-f5tgO%\n");
        }
        catch (ParseCancellationException ex){
            System.out.println(ex.getMessage());
        }

    }

}
