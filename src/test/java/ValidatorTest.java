import org.junit.Test;
import parser.CommandAndItsArgs;
import services.*;

import java.util.LinkedList;

public class ValidatorTest {
    @Test
    public void isValid() throws ValidatorException {
        AnalyzerExistingCommands.analyze();

        CommandAndItsArgs commandAndItsArgs = new CommandAndItsArgs();
        commandAndItsArgs.args = new LinkedList<>();
        iStorage storage = new Storage();

        // Bad situations
        // 1) Команды не существует
        String[] badCommands = {"", "bad", "\n", "PU SH", "1", "1\n"};
        for (String badCommand : badCommands) {
            commandAndItsArgs.commandName = badCommand;
            try {
                PreCompValidator.isValid(commandAndItsArgs, storage);
            } catch (ValidatorException ex){
                System.out.println(ex.getMessage());
            }
        }
        // 2) Неверное число аргументов
        commandAndItsArgs.commandName = "PUSH";
        try {
            PreCompValidator.isValid(commandAndItsArgs, storage);
        } catch (ValidatorException ex){
            System.out.println(ex.getMessage());
        }
        commandAndItsArgs.args.add("1");
        commandAndItsArgs.args.add("2");
        try {
            PreCompValidator.isValid(commandAndItsArgs, storage);
        } catch (ValidatorException ex){
            System.out.println(ex.getMessage());
        }
        commandAndItsArgs.args.clear();
        commandAndItsArgs.commandName = "POP";
        commandAndItsArgs.args.add("1");
        try {
            PreCompValidator.isValid(commandAndItsArgs, storage);
        } catch (ValidatorException ex){
            System.out.println(ex.getMessage());
        }
        commandAndItsArgs.args.add("1");
        try {
            PreCompValidator.isValid(commandAndItsArgs, storage);
        } catch (ValidatorException ex){
            System.out.println(ex.getMessage());
        }
        commandAndItsArgs.args.clear();
        // 3) На стеке недостаточно аргументов
        commandAndItsArgs.commandName = "POP";
        try {
            PreCompValidator.isValid(commandAndItsArgs, storage);
        } catch (ValidatorException ex){
            System.out.println(ex.getMessage());
        }
        // 4) Нет параметра в дефайн листе
        commandAndItsArgs.commandName = "PUSH";
        commandAndItsArgs.args.add("a");
        try {
            PreCompValidator.isValid(commandAndItsArgs, storage);
        } catch (ValidatorException ex){
            System.out.println(ex.getMessage());
        }
        commandAndItsArgs.args.clear();
        // Все в порядке
        commandAndItsArgs.commandName = "PUSH";
        commandAndItsArgs.args.add("4");
        assert (PreCompValidator.isValid(commandAndItsArgs, storage));
        commandAndItsArgs.args.clear();
        commandAndItsArgs.commandName = "POP";
        storage.pushStack(1);
        assert (PreCompValidator.isValid(commandAndItsArgs, storage));
        storage.defineWordAsNumber("b", 5);
        commandAndItsArgs.commandName = "PUSH";
        commandAndItsArgs.args.add("b");
        assert (PreCompValidator.isValid(commandAndItsArgs, storage));


    }
}
